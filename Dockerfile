#FROM registry.cern.ch/docker.io/library/almalinux:9
FROM gitlab-registry.cern.ch/linuxsupport/alma9-base:latest

RUN dnf install -y 'dnf-command(config-manager)' \
 && dnf config-manager --set-enabled crb \
 && dnf install -y https://linuxsoft.cern.ch/wlcg/el9/x86_64/wlcg-repo-1.0.0-1.el9.noarch.rpm \
                   https://dl.fedoraproject.org/pub/epel/epel-release-latest-9.noarch.rpm \
 && dnf install -y doxygen graphviz python \
 && dnf install -y git \
 && dnf install -y zip  \
 && dnf install -y sshpass \
 && dnf install -y unzip \
 && dnf install -y epel-release \
 && dnf install -y p7zip p7zip-plugins \
 && dnf clean all && rm -rf /var/cache/dnf

COPY eos9-x86_64.repo /etc/yum.repos.d/eos9-x86_64.repo
RUN dnf install -y eos-client \
 && dnf clean all && rm -rf /var/cache/dnf

